import axiosInstance from "./index.ts";

export type GetAllNotesRequest = {
  token: string;
};

export type GetAllNotesResponse = {
  notes: Note[];
};
type Note = {
  id: number;
  title: string;
  content: string;
};

export const getAllNotesApi = async ([data]: [
  GetAllNotesRequest
]): Promise<GetAllNotesResponse> => {
  const response = await axiosInstance.get<GetAllNotesResponse>("/note", {
    headers: {
      "X-SESSION-ID": data.token,
    },
  });
  return response.data;
};

export type AddNotesRequest = {
  token: string;
  title: string;
  content: string;
};
export type AddNotesResponse = {
  id: number;
  title: string;
  content: string;
};

export const addNotesApi = async ([data]: [
  AddNotesRequest
]): Promise<AddNotesResponse> => {
  const { token, ...body } = data;
  const result = await axiosInstance.post<AddNotesResponse>("/note", body, {
    headers: {
      "X-SESSION-ID": token,
    },
  });
  return result.data;
};

export type EditNotesRequest = {
  id: number;
  token: string;
  title: string;
  content: string;
};
export type EditNotesResponse = {
  id: number;
  title: string;
  content: string;
};

export const editNotesApi = async ([data]: [
  EditNotesRequest
]): Promise<EditNotesResponse> => {
  const { token, id, ...body } = data;
  const partialBody: Partial<EditNotesRequest> = {};

  if (body.title) {
    partialBody.title = body.title;
  }
  if (body.content) {
    partialBody.content = body.content;
  }

  const result = await axiosInstance.patch<EditNotesResponse>(
    `/note/${id}`,
    partialBody,
    {
      headers: {
        "X-SESSION-ID": token,
      },
    }
  );
  return result.data;
};
export type DeleteNotesRequest = {
  id: number;
  token: string;
};
export type DeleteNotesResponse = {
  message: string;
};
export const deleteNotesApi = async ([data]: [
  DeleteNotesRequest
]): Promise<DeleteNotesResponse> => {
  const { token, id } = data;

  const result = await axiosInstance.delete<DeleteNotesResponse>(
    `/note/${id}`,
    {
      headers: {
        "X-SESSION-ID": token,
      },
    }
  );
  return result.data;
};
