import axios from "axios";

const urls = {
  dotnet8: "https://localhost:44357/api/v1/",
};

const axiosInstance = axios.create({
  baseURL: urls["dotnet8"],
});

export default axiosInstance;
