import axiosInstance from "./index.ts";

export type LoginApiRequest = {
  email: string;
  password: string;
};
export type LoginApiResponse = {
  token: string;
  email: string;
};
export const loginApi = async ([data]: [
  LoginApiRequest
]): Promise<LoginApiResponse> => {
  const result = await axiosInstance.post<LoginApiResponse>(
    "/auth/login",
    data
  );
  return result.data;
};

export type RegisterApiRequest = {
  email: string;
  password: string;
};
export type RegisterApiResponse = {
  id: number;
  email: string;
};
export const registerApi = async ([data]: [
  RegisterApiRequest
]): Promise<RegisterApiResponse> => {
  const result = await axiosInstance.post<RegisterApiResponse>(
    "/auth/register",
    data
  );
  return result.data;
};

export type LogoutApiRequest = {
  token: string;
};
export type LogoutApiResponse = {
  message: string;
};
export const logoutApi = async ([data]: [
  LogoutApiRequest
]): Promise<LogoutApiResponse> => {
  const result = await axiosInstance.get<LogoutApiResponse>("/auth/logout", {
    headers: {
      "X-SESSION-ID": data.token,
    },
  });
  return result.data;
};
