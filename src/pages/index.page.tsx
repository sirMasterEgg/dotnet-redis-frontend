const IndexPage = () => {
  return (
    <div className="h-screen flex flex-col items-center justify-center gap-4">
      <h1 className="text-2xl">Sharing Session</h1>
      <a
        href="/login"
        className="w-24 text-center px-4 py-2 bg-black rounded text-white"
      >
        Login
      </a>
      <a
        href="/register"
        className="w-24 text-center px-4 py-2 rounded border border-black"
      >
        Register
      </a>
    </div>
  );
};
export default IndexPage;
