import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { loginApi } from "../apis/auth.ts";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

type LoginForm = {
  email: string;
  password: string;
};

const LoginPage = () => {
  const { register, handleSubmit } = useForm<LoginForm>();
  const navigate = useNavigate();

  const loginMutation = useMutation({
    mutationFn: loginApi,
    onSuccess: (result) => {
      toast.success("Login success");
      localStorage.setItem("SESSID", result.token);
      localStorage.setItem("SESSMAIL", result.email);
      navigate("/dashboard", { replace: true });
    },
    onError: () => {
      toast.error("Login failed");
    },
  });

  const onSubmit = (data: LoginForm) => {
    loginMutation.mutate([
      {
        email: data.email,
        password: data.password,
      },
    ]);
  };

  return (
    <div className="h-screen flex flex-col items-center justify-center">
      <h1 className="text-2xl my-2">Login</h1>
      <form
        className="flex flex-col gap-3 w-1/4"
        onSubmit={handleSubmit(onSubmit)}
      >
        <input
          type="text"
          className="rounded"
          placeholder="Email"
          {...register("email")}
        />
        <input
          type="password"
          className="rounded"
          placeholder="Password"
          {...register("password")}
        />
        <input
          type="submit"
          value="Login"
          className="w-full text-center px-4 py-2 bg-black rounded text-white"
        />
        <a href="/register" className="text-center underline text-blue-600">
          To Register
        </a>
      </form>
    </div>
  );
};
export default LoginPage;
