import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { logoutApi } from "../../apis/auth.ts";
import { toast } from "react-toastify";
import {
  addNotesApi,
  deleteNotesApi,
  editNotesApi,
  getAllNotesApi,
} from "../../apis/note.ts";
import { useForm } from "react-hook-form";

type AddNoteForm = {
  title: string;
  content: string;
};
type EditNoteForm = {
  id: number;
  title: string;
  content: string;
};

const DashboardPage = () => {
  const navigate = useNavigate();
  const [formShow, setFormShow] = useState<"hide" | "edit" | "create">("hide");

  const {
    register: registerAdd,
    handleSubmit: handleSubmitAdd,
    reset: resetAdd,
  } = useForm<AddNoteForm>();
  const {
    register: registerEdit,
    handleSubmit: handleSubmitEdit,
    reset: resetEdit,
    setValue: setValueEdit,
  } = useForm<EditNoteForm>();

  useEffect(() => {
    if (localStorage.getItem("SESSID") === null) {
      navigate("/login", { replace: true });
    }
  }, []);

  const submitAdd = (data: AddNoteForm) => {
    addMutation.mutate([
      {
        token: localStorage.getItem("SESSID") ?? "",
        ...data,
      },
    ]);
  };
  const submitEdit = (data: EditNoteForm) => {
    editMutation.mutate([
      {
        token: localStorage.getItem("SESSID") ?? "",
        ...data,
      },
    ]);
  };

  const addMutation = useMutation({
    mutationFn: addNotesApi,
    onSuccess: async () => {
      await refetch();
      toast.success("Success add note!");
      resetAdd();
      setFormShow("hide");
    },
    onError: () => {
      toast.error("Failed to add note!");
    },
  });
  const editMutation = useMutation({
    mutationFn: editNotesApi,
    onSuccess: async () => {
      await refetch();
      toast.success("Success edit note!");
      resetEdit();
      setFormShow("hide");
    },
    onError: () => {
      toast.error("Failed to edit note!");
    },
  });

  const deleteMutation = useMutation({
    mutationFn: deleteNotesApi,
    onSuccess: async () => {
      await refetch();
      toast.success("Successfully delete note");
    },
    onError: () => {
      toast.error("Delete note failed");
    },
  });

  const logoutMutation = useMutation({
    mutationFn: logoutApi,
    onSuccess: () => {
      toast.success("Logout success");
      localStorage.removeItem("SESSID");
      localStorage.removeItem("SESSMAIL");
      navigate("/login", { replace: true });
    },
    onError: () => {
      toast.error("Logout failed");
    },
  });

  const handleLogout = () => {
    logoutMutation.mutate([
      {
        token: localStorage.getItem("SESSID") ?? "",
      },
    ]);
  };

  const handleDelete = (id: number) => {
    deleteMutation.mutate([
      {
        token: localStorage.getItem("SESSID") ?? "",
        id: id,
      },
    ]);
  };

  const handleFormShow = () => {
    if (formShow === "hide") {
      setFormShow("create");
      return;
    }
    if (formShow === "create" || formShow === "edit") {
      setFormShow("hide");
      return;
    }
  };

  const { data, refetch } = useQuery({
    queryKey: ["notes", localStorage.getItem("SESSID") ?? ""],
    queryFn: (key) =>
      getAllNotesApi([
        {
          token: key.queryKey[1],
        },
      ]),
  });

  const handleEdit = (old: EditNoteForm) => {
    setFormShow("edit");
    setValueEdit("id", old.id);
    setValueEdit("title", old.title);
    setValueEdit("content", old.content);
  };

  return (
    <div className="max-h-screen px-2">
      <h1 className="text-2xl my-2">
        Dashboard ({localStorage.getItem("SESSMAIL")})
      </h1>
      <div className="flex flex-row gap-2 my-2">
        <button
          onClick={handleFormShow}
          className="text-center px-2 border border-black rounded"
        >
          {formShow === "hide" ? "Add Note" : "Hide"}
        </button>
        <button
          onClick={handleLogout}
          className="text-center px-2 py-1 bg-black rounded text-white"
        >
          Logout
        </button>
      </div>
      <div className="flex flex-row gap-2">
        <table className="table-auto border border-slate-500">
          <thead>
            <tr>
              <th className="border border-slate-600">No</th>
              <th className="border border-slate-600">Title</th>
              <th className="border border-slate-600">Content</th>
              <th className="border border-slate-600">Action</th>
            </tr>
          </thead>
          <tbody>
            {data?.notes.map((note, idx) => {
              return (
                <tr key={crypto.randomUUID()}>
                  <td className="border border-slate-600 px-2">{++idx}</td>
                  <td className="border border-slate-600 px-2">{note.title}</td>
                  <td className="border border-slate-600 px-2">
                    {note.content}
                  </td>
                  <td className="border border-slate-600 p-1">
                    <button
                      onClick={() =>
                        handleEdit({
                          id: note.id,
                          content: note.content,
                          title: note.title,
                        })
                      }
                      className="text-center px-2 bg-black rounded text-white mr-1"
                    >
                      Edit
                    </button>
                    <button
                      onClick={() => handleDelete(note.id)}
                      className="text-center px-2 border border-black rounded"
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        {formShow === "hide" ? (
          <></>
        ) : (
          <div className="flex flex-col w-1/4 mx-5">
            <h1 className="capitalize text-2xl font-bold">{formShow}</h1>
            {formShow === "edit" ? (
              <form
                className="flex flex-col gap-1 w-full"
                onSubmit={handleSubmitEdit(submitEdit)}
              >
                <input
                  readOnly
                  placeholder="ID"
                  className="rounded"
                  {...registerEdit("id")}
                />
                <input
                  type="text"
                  placeholder="Title"
                  className="rounded"
                  {...registerEdit("title")}
                />
                <input
                  type="text"
                  placeholder="Content"
                  className="rounded"
                  {...registerEdit("content")}
                />
                <input
                  type="submit"
                  value="Submit"
                  className="w-full text-center px-4 py-2 bg-black rounded text-white"
                />
              </form>
            ) : (
              <form
                className="flex flex-col gap-1 w-full"
                onSubmit={handleSubmitAdd(submitAdd)}
              >
                <input
                  type="text"
                  placeholder="Title"
                  {...registerAdd("title")}
                  className="rounded"
                />
                <input
                  type="text"
                  placeholder="Content"
                  {...registerAdd("content")}
                  className="rounded"
                />
                <input
                  type="submit"
                  value="Submit"
                  className="w-full text-center px-4 py-2 bg-black rounded text-white"
                />
              </form>
            )}
          </div>
        )}
      </div>
    </div>
  );
};
export default DashboardPage;
