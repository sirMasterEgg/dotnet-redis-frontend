const NotFoundPage = () => {
  return (
    <div className="h-screen flex flex-col items-center justify-center">
      <h1 className="text-3xl">404 - Page Not Found</h1>
    </div>
  );
};
export default NotFoundPage;
