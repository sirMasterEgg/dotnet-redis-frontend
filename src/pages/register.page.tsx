import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { registerApi } from "../apis/auth.ts";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

type RegisterForm = {
  email: string;
  password: string;
};
const RegisterPage = () => {
  const { register, handleSubmit } = useForm<RegisterForm>();
  const navigate = useNavigate();

  const registerMutation = useMutation({
    mutationFn: registerApi,
    onSuccess: () => {
      toast.success("Register success");
      navigate("/login");
    },
    onError: () => {
      toast.error("Register failed");
    },
  });

  const onSubmit = (data: RegisterForm) => {
    registerMutation.mutate([
      {
        email: data.email,
        password: data.password,
      },
    ]);
  };

  return (
    <div className="h-screen flex flex-col items-center justify-center">
      <h1 className="text-2xl my-2">Register</h1>
      <form
        className="flex flex-col gap-3 w-1/4"
        onSubmit={handleSubmit(onSubmit)}
      >
        <input
          type="text"
          className="rounded"
          placeholder="Email"
          {...register("email")}
        />
        <input
          type="password"
          className="rounded"
          placeholder="Password"
          {...register("password")}
        />
        <input
          type="submit"
          value="Register"
          className="w-full text-center px-4 py-2 bg-black rounded text-white"
        />
        <a href="/login" className="text-center underline text-blue-600">
          To Login
        </a>
      </form>
    </div>
  );
};
export default RegisterPage;
